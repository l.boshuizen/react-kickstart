import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, Route, NavLink, Switch } from 'react-router-dom';

import { Main } from './views/main';

require('bootstrap/dist/css/bootstrap.min.css')
require('./app.css')

type LinkProps = {
    url: string;
    label: string;
    isRoot? : boolean;
}

class DecoratedLink extends React.Component<LinkProps,any>{
    render(){

        const exact = this.props.isRoot || false; 

        return(
            <li className="nav-item"><NavLink className="nav-link" activeClassName="active" exact={exact} to={this.props.url}>{this.props.label}</NavLink></li>
        );
    }
}

const navBarStyle = {
    padding: "10px"
}

class NavigationBar extends React.Component<any,any>{
    render(){
        return(
            <ul className="nav flex-column nav-pills" style={ navBarStyle }>
                <DecoratedLink url="/" label="Overview" isRoot={ true } />
            </ul>
        );
    }
}

const headerHeight = {
    height: "50px",
    maxHeight: "50px",
    borderBottom: "solid 1px black",
    marginBottom: "10px"
};

const styleFooter = {
    borderTop: "solid 1px black",
    position: "absolute",
    bottom: "0",
    width: "100%",
    height: "30px",  // Set the fixed height of the footer here
    lineHeight: "30px" // Vertically center the text there //
};

const appAreaStyle = {
    paddingBottom: "30px"
}

const appNameStyle = {
    ...headerHeight,
    paddingLeft: "10px"
}

class AppName extends React.Component<any,any>{
    render(){
        return(
            <div style={ appNameStyle }>
                <h1>App</h1>
            </div>
        )
    }
}

class Sidebar extends React.Component {
    render(){
        return(
            <div className='sidebar'>
                <AppName />
                <NavigationBar />
            </div>
        );
    }
}

class Header extends React.Component<any,any>{
    render() {
        return(
            <div style={ headerHeight }>
                <p>Header</p>
            </div>
        )
    }
}

class Footer extends React.Component<any,any>{
    render() {
        return(
            <footer>
                <div className='container-fluid' style={ styleFooter }>
                    <p>&copy; Copyright</p>
                </div>
            </footer>
        )
    }
}

class NotImplemented extends React.Component<any,any>{
    render(){
        return(
            <div className="alert alert-warning" role="alert">
                <p>Not implemented (yet)</p>
            </div>
        );
    }
}

class App extends React.Component<any,any>{

    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className='col-md-2' >
                        <Sidebar />
                    </div>
                    <div className='col-md-10'>
                        <Header />
                        <div className="app-area" style={appAreaStyle}>
                            <Switch>
                                <Route exact path='/' component={Main} />
                            </Switch>
                        </div>
                        <Footer />
                    </div>
                </div>
            </div>
        );
    }

}

class Wrapper extends React.Component<any,any> {

    render(){
        return(
            <BrowserRouter>
            <App />
            </BrowserRouter>
        );
    }

}

ReactDOM.render( <Wrapper />, document.getElementById('app'));
